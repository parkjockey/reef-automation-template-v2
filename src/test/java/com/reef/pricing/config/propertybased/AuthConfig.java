package com.reef.pricing.config.propertybased;

import org.aeonbits.owner.Config;

import static com.reef.pricing.config.propertybased.OwnerConstant.VALUE_NOT_FOUND;
import static com.reef.pricing.constant.CommandLineProperty.Constants.TARGET_ENV;

@Config.Sources("file:${user.dir}/src/test/resources/properties/auth.properties")
@Config.LoadPolicy(Config.LoadType.MERGE)
public interface AuthConfig extends Config {

    @DefaultValue("Username " + VALUE_NOT_FOUND)
    @Key("basic.username")
    String username();

    @DefaultValue("Password " + VALUE_NOT_FOUND)
    @Key("basic.password")
    String password();

    @DefaultValue("Token " + VALUE_NOT_FOUND)
    @Key("${" + TARGET_ENV + "}.access")
    String accessToken();
}