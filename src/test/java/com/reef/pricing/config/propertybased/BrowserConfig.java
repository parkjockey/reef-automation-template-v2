package com.reef.pricing.config.propertybased;

import org.aeonbits.owner.Config;

import static com.reef.pricing.config.propertybased.OwnerConstant.VALUE_NOT_FOUND;

@Config.Sources("file:${user.dir}/src/test/resources/browser-config.properties")
@Config.LoadPolicy(Config.LoadType.MERGE)
public interface BrowserConfig extends Config {

    @DefaultValue("Headless " + VALUE_NOT_FOUND)
    @Key("headless")
    boolean headless();

    @DefaultValue("Default browser " + VALUE_NOT_FOUND)
    @Key("default")
    String defaultBrowser();
}