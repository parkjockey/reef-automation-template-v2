package com.reef.pricing.config.propertybased;

import org.aeonbits.owner.ConfigCache;

import static com.reef.pricing.constant.CommandLineProperty.Constants.TARGET_ENV;
import static org.aeonbits.owner.util.Collections.map;

public class ConfigurationManager {

    // prevent instantiation
    private ConfigurationManager() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    // following method allows us to do dynamic property lookup via system property e.g. "${TARGET_ENV}.accessToken"
    public static AuthConfig getAuthConfigInstance() { return ConfigCache.getOrCreate(AuthConfig.class, map(TARGET_ENV, System.getProperty(TARGET_ENV))); }
    public static BrowserConfig getBrowserConfigInstance() { return ConfigCache.getOrCreate(BrowserConfig.class); }
    public static TestrailConfig getTestrailConfigInstance() { return ConfigCache.getOrCreate(TestrailConfig.class); }
}