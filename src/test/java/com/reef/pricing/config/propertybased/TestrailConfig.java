package com.reef.pricing.config.propertybased;

import org.aeonbits.owner.Config;

import static com.reef.pricing.config.propertybased.OwnerConstant.VALUE_NOT_FOUND;

@Config.Sources({ "file:${user.dir}/src/test/resources/properties/testrail.properties" })
public interface TestrailConfig extends Config {

    @DefaultValue("Username " + VALUE_NOT_FOUND)
    @Key("username")
    String username();

    @DefaultValue("Password " + VALUE_NOT_FOUND)
    @Key("password")
    String password();

    @DefaultValue("Token " + VALUE_NOT_FOUND)
    @Key("pricing.engine.project.id")
    int projectId();
}