package com.reef.pricing.config.yamlbased;

public enum EnvironmentProfileName {

    QA(Constants.QA),
    DEV(Constants.DEV),
    STAGE(Constants.STAGE),
    PROD(Constants.PROD);

    EnvironmentProfileName(String string) {
        if(!string.equals(this.name()))
            throw new IllegalArgumentException();
    }

    public static final class Constants {
        // prevent instantiation
        private Constants() {
            throw new AssertionError("Instantiation attempted from within class");
        }

        public static final String QA = "qa";
        public static final String DEV = "dev";
        public static final String STAGE = "stage";
        public static final String PROD = "prod";
    }
}