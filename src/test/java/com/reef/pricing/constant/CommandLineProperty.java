package com.reef.pricing.constant;

public enum CommandLineProperty {

    TARGET_ENV(Constants.TARGET_ENV),
    TESTRAIL_ENABLED(Constants.TESTRAIL_RUN_ENABLED),
    PARALLEL(Constants.PARALLEL);

    CommandLineProperty(String string) {
        if(!string.equals(this.name()))
            throw new IllegalArgumentException();
    }

    public static final class Constants {
        // prevent instantiation
        private Constants() {
            throw new AssertionError("Instantiation attempted from within class");
        }

        public static final String TARGET_ENV = "target_env";
        public static final String TESTRAIL_RUN_ENABLED = "testrail_enabled";
        public static final String PARALLEL = "parallel_enabled";
    }
}