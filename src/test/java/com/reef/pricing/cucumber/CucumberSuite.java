package com.reef.pricing.cucumber;

import io.cucumber.junit.platform.engine.Cucumber;

// following annotation is deprecated in 7+ versions of cucumber and should use the @Suite annotation
@Cucumber
public class CucumberSuite { }