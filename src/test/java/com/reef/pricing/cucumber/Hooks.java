package com.reef.pricing.cucumber;

import com.reef.pricing.config.propertybased.ConfigurationManager;
import com.reef.pricing.helpers.annotationprocessor.CucumberTagExtractor;
import com.reef.pricing.helpers.annotationprocessor.TagExtractor;
import com.reef.pricing.report.AllureManager;
import com.reef.pricing.testrail.TestrailCaseStatus;
import com.reef.pricing.testrail.TestrailHelpers;
import com.reef.pricing.webdriver.ThreadLocalDriver;
import com.reef.pricing.webdriver.WebDriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

import java.io.IOException;
import java.util.Locale;

public class Hooks {

    private static boolean isCloseTestrailRunEnabled = false;
    private static boolean isCreateTestrailRunEnabled = false;
    private static final boolean isAddTestrailCasesToCurrentRunEnabled = false;
    private static final String DEFAULT_ENVIRONMENT = "qa";

    @Before
    public static void beforeTestRunActions() {
        if (isCloseTestrailRunEnabled) {
            Runtime.getRuntime().addShutdownHook(new Thread(TestrailHelpers::closeRun));
            isCloseTestrailRunEnabled = false;
        }

        if (isCreateTestrailRunEnabled) {
            TestrailHelpers.setTestrailRunWithBaseName("Automated run: ");
            isCreateTestrailRunEnabled = false;
        }
    }

    @Before("@ui")
    public void beforeUiScenario() {
        String defaultBrowser = ConfigurationManager.getBrowserConfigInstance().defaultBrowser().toUpperCase(Locale.ROOT);
        if (ThreadLocalDriver.getDriver() == null) {
            ThreadLocalDriver.addDriver(WebDriverFactory.valueOf(defaultBrowser).createDriver());
        }
    }

    @After
    public void afterStep(Scenario scenario) {
        TagExtractor extractor = new CucumberTagExtractor(scenario);
        int caseId = extractor.extractByTagName("testrail");

        if (isAddTestrailCasesToCurrentRunEnabled) {
            TestrailHelpers.addCaseToRun(caseId);
            if (scenario.isFailed()) {
                TestrailHelpers.addResultWithStatusToCase(TestrailCaseStatus.FAIL.getStatus(), caseId);
            } else TestrailHelpers.addResultWithStatusToCase(TestrailCaseStatus.SUCCESS.getStatus(), caseId);
        }
    }

    @After("@ui")
    public void afterUiScenario(Scenario scenario) throws IOException {
        if (scenario.isFailed()) {
            AllureManager.takeScreenshotToAttachOnAllureReport(scenario.getName());
        }

        // ensure browser instance is killed
        if (ThreadLocalDriver.getDriver() != null) {
            ThreadLocalDriver.getDriver().quit();
        }
    }
}