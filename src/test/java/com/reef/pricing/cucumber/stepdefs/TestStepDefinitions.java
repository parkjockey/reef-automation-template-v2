package com.reef.pricing.cucumber.stepdefs;

import com.reef.pricing.model.ExampleCucumberContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;

public class TestStepDefinitions {

    private final ExampleCucumberContext exampleCucumberContext;

    public TestStepDefinitions(ExampleCucumberContext exampleCucumberContext) {
        this.exampleCucumberContext = exampleCucumberContext;
    }

    @Given("^.* has (\\d+) cukes$")
    public void johnHasCukes$(int initialCount) {
        exampleCucumberContext.setCount(initialCount);
        System.out.println("John should have: " + exampleCucumberContext.getCount() + " cukes");
    }

    @And("John adds <{int}> cukes")
    public void johnAddsCukes(int count) {
        exampleCucumberContext.add(count);
    }

    @Then("John has <{int}> cukes in total")
    public void johnHasCukesInTotal(int expectedTotal) {
        final int actualTotal = exampleCucumberContext.getCount();
        System.out.println("Igorrr, it's aliveeee! Current total is: " + actualTotal );
        Assertions.assertEquals(actualTotal, expectedTotal);
    }
}