package com.reef.pricing.helpers.annotationprocessor;

import io.cucumber.java.Scenario;

public class CucumberTagExtractor implements TagExtractor {

    private final Scenario scenario;

    public CucumberTagExtractor(Scenario scenario) {
        this.scenario = scenario;
    }

    @Override
    public int extractByTagName(String tag) {
        int nonExistentTagValue = 0;
        for (String tagName : scenario.getSourceTagNames()) {
            if (tagName.contains(tag)) {
                String fromOpenParenthesis = tagName.substring(tagName.indexOf("(") + 1);
                String betweenParenthesis = fromOpenParenthesis.substring(0, fromOpenParenthesis.indexOf(")"));
                return Integer.parseInt(betweenParenthesis);
            }
        }
        return nonExistentTagValue;
    }
}