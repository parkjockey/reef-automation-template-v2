package com.reef.pricing.helpers.annotationprocessor;

import com.reef.pricing.junit.annotations.Testrail;
import org.junit.jupiter.api.extension.ExtensionContext;

public class JunitTagExtractor implements TagExtractor {

    private final ExtensionContext context;

    public JunitTagExtractor(ExtensionContext context) {
        this.context = context;
    }

    @Override
    public int extractByTagName(String tag) {
        int testCaseId = 0;

        for (String tagName : context.getTags()) {
            if (tagName.contains(tag)) {
                testCaseId = context.getElement().orElseThrow().getAnnotation(Testrail.class).value();
            }
        }
        return testCaseId;
    }
}