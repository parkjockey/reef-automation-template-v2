package com.reef.pricing.helpers.annotationprocessor;

public interface TagExtractor {

    int extractByTagName(String tag);
}
