package com.reef.pricing.httpclient;

import com.reef.pricing.config.yamlbased.YamlConfigHandler;
import com.reef.pricing.config.propertybased.ConfigurationManager;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.*;
import java.util.Map;

public final class GraphqlClient {

    private static final String GRAPHQL_URL = YamlConfigHandler.getEnvironmentConfig().getMwGatewayUrl();

    // prevent instantiation
    private GraphqlClient() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    public static Response sendRequestWithTokenAuth(File file, String variables) throws IOException {
        final String accessToken = ConfigurationManager.getAuthConfigInstance().accessToken();

        Map<String, String> reefHeaders = Map.of(
                "Organization", "1",
                "Authorization", accessToken);

        String graphqlPayload = GraphqlHelpers.parseGraphql(file, variables);

        RequestSpecification rSpec = new RequestSpecBuilder()
                .setBaseUri(GRAPHQL_URL)
                .addHeaders(reefHeaders)
                .setBody(graphqlPayload)
                .build();
        return RestAssuredClientHelper.sendPostRequest(rSpec);
    }

    public static Response sendUnauthorizedRequest(File file, String variables) throws IOException {
        String graphqlPayload = GraphqlHelpers.parseGraphql(file, variables);

        RequestSpecification rSpec = new RequestSpecBuilder()
                .setBaseUri(GRAPHQL_URL)
                .setBody(graphqlPayload)
                .build();
        return RestAssuredClientHelper.sendPostRequest(rSpec);
    }
}