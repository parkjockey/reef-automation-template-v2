package com.reef.pricing.httpclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.*;

final class GraphqlHelpers {

    // prevent instantiation
    private GraphqlHelpers() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    static String parseGraphql(File file, String variables) throws IOException {
        String graphqlFileContent = convertInputStreamToString(new FileInputStream(file));
        return convertToGraphqlString(graphqlFileContent, variables);
    }

    static String convertToGraphqlString(String graphql, String variables) throws JsonProcessingException {
        ObjectMapper oMapper = new ObjectMapper();
        ObjectNode oNode = oMapper.createObjectNode();
        oNode.put("query", graphql);
        oNode.put("variables", variables);
        return oMapper.writeValueAsString(oNode);
    }

    static String convertInputStreamToString(InputStream inputStream) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }
        return sb.toString();
    }
}