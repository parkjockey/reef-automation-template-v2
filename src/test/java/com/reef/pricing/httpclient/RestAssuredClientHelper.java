package com.reef.pricing.httpclient;

import java.util.Map;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

final class RestAssuredClientHelper {

    // prevent instantiation
    private RestAssuredClientHelper() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    private static RequestSpecification commonRspec() {
        return given()
                .contentType(ContentType.JSON)
                .relaxedHTTPSValidation()
                .filter(new RestAssuredFilter());
    }

    static Response sendPostRequest(String username, String password, RequestSpecification rSpec) {
        return commonRspec()
                .auth()
                .preemptive()
                .basic(username, password)
                .spec(rSpec)
                .post()
                .then()
                .extract()
                .response();
    }

    static Response sendPostRequest(RequestSpecification rSpec) {
        return commonRspec()
                .spec(rSpec)
                .post()
                .then()
                .extract()
                .response();
    }

    static Response sendPostRequest(String URI, String username, String password) {
        return commonRspec()
                .auth()
                .basic(username, password)
                .post(URI)
                .then()
                .extract()
                .response();
    }

    public static Response sendGetRequestWithoutAuth(String URI, Map<String, String> queryParams) {
        return commonRspec()
                .queryParams(queryParams)
                .get(URI)
                .then()
                .extract()
                .response();
    }

    static Response sendGetRequestWithoutAuth(String URI) {
        return commonRspec()
                .get(URI)
                .then()
                .extract()
                .response();
    }

    static Response sendGetRequest(String URI, String username, String password, Map<String, String> parametersMap) {
        return commonRspec()
                .auth()
                .preemptive()
                .basic(username, password)
                .queryParams(parametersMap)
                .get(URI)
                .then()
                .extract()
                .response();
    }

    static Response sendGetRequest(String URI, String username, String password) {
        return commonRspec()
                .auth()
                .basic(username, password)
                .get(URI)
                .then()
                .extract()
                .response();
    }

    static Response sendRequest(RequestSpecification rSpec, String requestMethod) {

        return switch (requestMethod) {
            case "GET" -> commonRspec().spec(rSpec).get().then().extract().response();
            case "POST" -> commonRspec().spec(rSpec).post().then().extract().response();
            case "PUT" -> commonRspec().spec(rSpec).put().then().extract().response();
            case "DELETE" -> commonRspec().spec(rSpec).delete().then().extract().response();
            default -> throw new IllegalStateException(
                    "Unexpected value: " + requestMethod);
        };
    }
}