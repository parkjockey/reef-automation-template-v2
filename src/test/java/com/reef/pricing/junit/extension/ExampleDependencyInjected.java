package com.reef.pricing.junit.extension;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ExampleDependencyInjected {

    private String name;
}
