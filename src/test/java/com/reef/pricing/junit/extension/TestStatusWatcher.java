package com.reef.pricing.junit.extension;

import com.reef.pricing.junit.annotations.Testrail;
import com.reef.pricing.helpers.annotationprocessor.JunitTagExtractor;
import com.reef.pricing.helpers.annotationprocessor.TagExtractor;
import com.reef.pricing.junit.annotations.WIP;
import com.reef.pricing.report.AllureManager;
import com.reef.pricing.testrail.TestrailCaseStatus;
import com.reef.pricing.testrail.TestrailHelpers;
import lombok.SneakyThrows;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

import java.util.Locale;
import java.util.Optional;

public class TestStatusWatcher implements TestWatcher {

    private static boolean isCreateTestrailRunEnabled = Boolean.parseBoolean(System.getProperty("testrail_run_enabled"));
    private static final boolean isUpdateTestCaseResultEnabled = Boolean.parseBoolean(System.getProperty("testrail_case_enabled"));

    static {
        if (isCreateTestrailRunEnabled) {
            TestrailHelpers.setTestrailRunWithBaseName("Automated run: ");
            isCreateTestrailRunEnabled = false;
        }
    }

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        TestWatcher.super.testDisabled(context, reason);
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        if (isUpdateTestCaseResultEnabled) {
            updateByExtractedTestrailAnnotationTagValue(context, TestrailCaseStatus.SUCCESS.getStatus());
        }
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        TestWatcher.super.testAborted(context, cause);
    }

    @SneakyThrows
    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        if (isUpdateTestCaseResultEnabled) {
            updateByExtractedTestrailAnnotationTagValue(context, TestrailCaseStatus.FAIL.getStatus());
        }

        // generate screenshot for ui tests
        if (context.getTags().contains("ui")) {
            String failingTestName = context.getDisplayName().replaceAll(" ", "_");
            AllureManager.takeScreenshotToAttachOnAllureReport(failingTestName);
        }
    }

    private static void updateByExtractedTestrailAnnotationTagValue(ExtensionContext context, Integer status) {
        TagExtractor extractor = new JunitTagExtractor(context);
        String testrailTagName = Testrail.class.getName().toLowerCase(Locale.ROOT);
        int caseId = extractor.extractByTagName(testrailTagName);
        // 0 is the default value returned for tests that lack the specified tag
        if (caseId != 0) {
            TestrailHelpers.addCaseToRun(caseId);
            TestrailHelpers.addResultWithStatusToCase(status, caseId);
        }
    }
}