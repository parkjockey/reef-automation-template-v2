package com.reef.pricing.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExampleCucumberContext {

    private int count;

    public void add(int addition) {
        count += addition;
    }
}
