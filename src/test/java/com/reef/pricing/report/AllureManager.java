package com.reef.pricing.report;

import com.reef.pricing.webdriver.ThreadLocalDriver;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

public class AllureManager {

    // prevent instantiation
    private AllureManager() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    @Attachment(value = "Failed test screenshot", type = "image/png")
    public static void takeScreenshotToAttachOnAllureReport(String scenarioName) throws IOException {
        final String allureFilePath = "allure-results/screenshots/";
        File scrFile = ((TakesScreenshot) ThreadLocalDriver.getDriver()).getScreenshotAs(OutputType.FILE);
        File destinationFile = new File(allureFilePath + "screenshot_" + scenarioName + ".png");
        FileUtils.copyFile(scrFile, destinationFile);
    }

    @Attachment(value = "Browser information", type = "text/plain")
    public static String addBrowserInformationOnAllureReport() {
        return ThreadLocalDriver.getInfo();
    }
}