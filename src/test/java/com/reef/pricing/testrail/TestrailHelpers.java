package com.reef.pricing.testrail;

import com.codepine.api.testrail.TestRail;
import com.codepine.api.testrail.model.Result;
import com.codepine.api.testrail.model.ResultField;
import com.codepine.api.testrail.model.Run;
import com.reef.pricing.config.propertybased.ConfigurationManager;
import com.reef.pricing.config.propertybased.TestrailConfig;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigCache;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public final class TestrailHelpers {

    // prevent instantiation
    private TestrailHelpers() {
        throw new AssertionError("Instantiation attempted from within class");
    }

    private static TestRail testrailClient = null;
    private static Run testrailRun;
    private static final List<Integer> caseIds = new ArrayList<>();
    private static final TestrailConfig TESTRAIL_CONFIG = ConfigurationManager.getTestrailConfigInstance();

    public static void setTestrailRunWithBaseName(String name) {
        setTestrailClient();
        testrailRun = TestrailRunFactory.createForDefaultSuiteWithBaseName(name);
        addRunToClient(testrailRun);
    }

    public static void setTestrailClient() {
        String reefTestrailDomainUrl = "https://reef.testrail.io/";
        testrailClient = TestRail.builder(reefTestrailDomainUrl, TESTRAIL_CONFIG.username(), TESTRAIL_CONFIG.password())
                .build();
    }

    /**
     * @param run
     * @throws NullPointerException when the run is null
     * @throws IllegalArgumentException when the testrail project id is a negative number
     */
    public static void addRunToClient(Run run) {
        try {
            TestrailHelpers.testrailRun = testrailClient.runs().add(TESTRAIL_CONFIG.projectId(), run).execute();
        } catch (NullPointerException NPE){
            log.error("Run is null" + NPE.getMessage());
        } catch (IllegalArgumentException IAE) {
            log.error("ProjectId is not positive: " + IAE.getMessage());
        }
    }

    // This will add case id into current run
    public static void addCaseToRun(int caseId) {
        try {
            caseIds.add(caseId);
            testrailRun.setCaseIds(caseIds);
            testrailClient.runs().update(testrailRun).execute();
        } catch (Exception ex) {
            log.error("Can't add case to run " + ex.getMessage());
        }
    }

    // Function used to add final status with comment for a test case
    public static void addResultWithStatusToCase(int statusId, int caseId) {
        Result result = new Result().setStatusId(statusId);
        try {
            List<ResultField> customResultFields = testrailClient.resultFields().list().execute();
            testrailClient.results().addForCase(testrailRun.getId(), caseId, result, customResultFields).execute();
        } catch (IllegalArgumentException IAE) {
            log.error("Invalid runId or testCaseId: " + IAE.getMessage());
        }
    }
    
    public static void closeRun() {
        try {
            testrailClient.runs().close(testrailRun.getId()).execute();
        } catch (IllegalArgumentException IAE) {
            log.error("Invalid runId: " + IAE.getMessage());
        }
    }
}