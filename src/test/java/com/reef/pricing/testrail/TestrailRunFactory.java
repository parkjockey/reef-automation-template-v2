package com.reef.pricing.testrail;

import com.codepine.api.testrail.model.Run;
import com.reef.pricing.helpers.generators.DatetimeGenerator;

public class TestrailRunFactory {

    private static final int RMS_TESTRAIL_MOCK_SUITE_ID = 7375;
    private static final int RMS_TESTRAIL_SUITE_ID = 7443;

    public static Run createForDefaultSuiteWithBaseName(String runNameBase) {
        String datetimeString = DatetimeGenerator.getDatetimeOfPattern("dd MMM yyy kk mm s");
        String runNameWithDatetime = runNameBase + " " + datetimeString;
        return new Run()
                .setSuiteId(RMS_TESTRAIL_SUITE_ID)
                .setName(runNameWithDatetime)
                .setIncludeAll(false);
    }
}
