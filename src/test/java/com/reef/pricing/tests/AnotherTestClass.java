package com.reef.pricing.tests;

import com.reef.pricing.junit.annotations.WIP;
import com.reef.pricing.junit.extension.ExampleDependencyInjected;
import com.reef.pricing.junit.extension.ExampleDependencyInjector;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ExampleDependencyInjector.class)
class AnotherTestClass {

    private final ExampleDependencyInjected dependencyInjected;

    private AnotherTestClass(ExampleDependencyInjected dependencyInjected) {
        this.dependencyInjected = dependencyInjected;
    }

    @WIP
    @Test
    void constructor_level_dependency_injection_test() {
        Assertions.assertEquals("Worked!", dependencyInjected.getName(), "Names match");
    }
}
