package com.reef.pricing.tests;

import com.reef.pricing.config.yamlbased.YamlConfigHandler;
import com.reef.pricing.config.propertybased.ConfigurationManager;
import com.reef.pricing.junit.annotations.WIP;
import com.reef.pricing.junit.extension.ExampleDependencyInjected;
import com.reef.pricing.junit.extension.ExampleDependencyInjector;
import com.reef.pricing.webdriver.WebDriverFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfSystemProperty;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;

import java.util.Locale;

import static com.reef.pricing.config.yamlbased.EnvironmentProfileName.Constants.PROD;
import static com.reef.pricing.config.yamlbased.EnvironmentProfileName.Constants.QA;
import static com.reef.pricing.constant.CommandLineProperty.Constants.TARGET_ENV;

@ExtendWith(ExampleDependencyInjector.class)
@DisabledIfSystemProperty(named = TARGET_ENV, matches = PROD)
class TestClass {

    private static final String TEST_URL = "https://google.com";

    @Disabled
    @Test
    void owner_config_test() {
        Assertions.assertEquals("ownerTest", ConfigurationManager.getAuthConfigInstance().username());
    }

    @Disabled
    @Test
    void chrome_browser_test() {
        WebDriver driver = WebDriverFactory.CHROME.createDriver();
        driver.get(TEST_URL);
        Assertions.assertTrue(driver.getTitle().equalsIgnoreCase("google"));
        driver.close();
    }

    @Disabled
    @Test
    void firefox_browser_test() {
        final String desiredUrl = "https://yahoo.com";
        WebDriver driver = WebDriverFactory.FIREFOX.createDriver();
        driver.get(desiredUrl);
        Assertions.assertTrue(driver.getTitle().toLowerCase(Locale.ROOT).contains("yahoo"));
        driver.close();
    }

    @EnabledIfSystemProperty(named = TARGET_ENV, matches = QA)
    @Test
    void yaml_test() {
        final String originalQaGqlGatewayUrl = "https://internal-middleware-qa2.co.reefplatform.com/gateway/graphql";
        final String parsedQaGqlGatewayUrl = YamlConfigHandler.getEnvironmentConfig().getMwGatewayUrl();
        Assertions.assertEquals(originalQaGqlGatewayUrl, parsedQaGqlGatewayUrl);
    }

    @WIP
    @Test
    void method_level_dependency_injection_test(ExampleDependencyInjected injected) {
        Assertions.assertEquals("Worked!", injected.getName(), "Names match");
    }
}