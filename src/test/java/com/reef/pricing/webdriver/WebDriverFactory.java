package com.reef.pricing.webdriver;

import com.reef.pricing.exceptions.HeadlessNotSupportedException;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.AbstractDriverOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import static com.reef.pricing.config.propertybased.ConfigurationManager.getBrowserConfigInstance;

public enum WebDriverFactory {

    CHROME {
        @Override
        public RemoteWebDriver createDriver() {
            WebDriverManager.getInstance(DriverManagerType.CHROME).setup();

            return new ChromeDriver(getOptions());
        }

        @Override
        public ChromeOptions getOptions() {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments(START_MAXIMIZED);
            chromeOptions.addArguments(DISABLE_INFOBARS);
            chromeOptions.addArguments(DISABLE_NOTIFICATIONS);
            chromeOptions.setHeadless(getBrowserConfigInstance().headless());

            return chromeOptions;
        }
    }, FIREFOX {
        @Override
        public RemoteWebDriver createDriver() {
            WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();

            return new FirefoxDriver(getOptions());
        }

        @Override
        public FirefoxOptions getOptions() {
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.addArguments(START_MAXIMIZED);
            firefoxOptions.setHeadless(getBrowserConfigInstance().headless());

            return firefoxOptions;
        }
    }, OPERA {
        @Override
        public RemoteWebDriver createDriver() {
            WebDriverManager.getInstance(DriverManagerType.OPERA).setup();

            return new OperaDriver(getOptions());
        }

        @Override
        public OperaOptions getOptions() {
            OperaOptions operaOptions = new OperaOptions();
            operaOptions.addArguments(START_MAXIMIZED);
            operaOptions.addArguments(DISABLE_INFOBARS);
            operaOptions.addArguments(DISABLE_NOTIFICATIONS);

            if (getBrowserConfigInstance().headless()) {
                throw new HeadlessNotSupportedException(operaOptions.getBrowserName());
            }

            return operaOptions;
        }
    };

    private static final String START_MAXIMIZED = "--start-maximized";
    private static final String DISABLE_NOTIFICATIONS = "--disable-notifications";
    private static final String DISABLE_INFOBARS = "--disable-infobars";

    public abstract RemoteWebDriver createDriver();

    public abstract AbstractDriverOptions<?> getOptions();
}